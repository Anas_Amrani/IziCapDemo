package com.Izicap.HomeWorkProject.mapper;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;
import com.Izicap.HomeWorkProject.dto.ResponseDto;

@Component
public class ResponseMapper {
	
	public CustomisedResponse responseCutomiser(ResponseDto dto) {
		String adressComplement = "";
		if (dto.getEstablishement().getAdressComplement()  !=null ) {
			adressComplement = dto.getEstablishement().getAdressComplement();
		}
		CustomisedResponse customisedResponse = new CustomisedResponse();
		customisedResponse.setId(dto.getEstablishement().getId());
		customisedResponse.setNic(dto.getEstablishement().getNic());
		customisedResponse.setFullAdress(dto.getEstablishement().getGeoAdress() + adressComplement);
		customisedResponse.setCreationDate(new SimpleDateFormat("yyyy/MM/dd").format(dto.getEstablishement().getCreationDate()));
		customisedResponse.setName(dto.getEstablishement().getLegalUnity().getDenomination());
		customisedResponse.setTvaNumber(dto.getEstablishement().getLegalUnity().getTvaNumber());
		return customisedResponse;
	}
}
