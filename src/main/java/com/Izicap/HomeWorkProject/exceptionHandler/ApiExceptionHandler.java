package com.Izicap.HomeWorkProject.exceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.Izicap.HomeWorkProject.dto.ErrorsDto;

@ControllerAdvice
public class ApiExceptionHandler {

	@ExceptionHandler(value = {ApiResponseException.class})
	public ResponseEntity<Object> handleApiException(ApiResponseException exc){
		ErrorsDto errors = new ErrorsDto(exc.getMessage(), HttpStatus.NOT_FOUND, ZonedDateTime.now(ZoneId.of("Europe/Paris")));
		return new ResponseEntity<Object>(errors,HttpStatus.NOT_FOUND);
	}
}
