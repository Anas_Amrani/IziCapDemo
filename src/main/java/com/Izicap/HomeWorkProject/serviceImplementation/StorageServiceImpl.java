package com.Izicap.HomeWorkProject.serviceImplementation;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class StorageServiceImpl implements com.Izicap.HomeWorkProject.service.StorageService {

	@Override
	public void saveResponseInCsvFile(List<CustomisedResponse> response) throws IOException {
   	 File file = new File("src/main/resources/generated/companiesInfos.csv");
	    try {
	        FileWriter outputfile = new FileWriter(file);
	        CSVWriter writer = new CSVWriter(outputfile, ';',
                 CSVWriter.NO_QUOTE_CHARACTER,
                 CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                 CSVWriter.DEFAULT_LINE_END);
	        String[] header = { "Id", "Nic", "Full Adress", "Creation Date", "Full Name", "TVA Number" };
	        writer.writeNext(header);
	        for (CustomisedResponse data : response) {
	        	String[] dataLigne = {	data.getId(),
						data.getNic(),
						data.getFullAdress(),
						data.getCreationDate(),
						data.getName(),
						data.getTvaNumber() } ;
						writer.writeNext(dataLigne);
			}
	        writer.close();
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	        throw e ;
	    }
		
	}
}
