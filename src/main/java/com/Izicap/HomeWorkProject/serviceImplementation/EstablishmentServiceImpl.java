package com.Izicap.HomeWorkProject.serviceImplementation;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;
import com.Izicap.HomeWorkProject.dto.RequestDto;
import com.Izicap.HomeWorkProject.dto.ResponseDto;
import com.Izicap.HomeWorkProject.exceptionHandler.ApiResponseException;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@NoArgsConstructor
@Data
@Service
public class EstablishmentServiceImpl implements com.Izicap.HomeWorkProject.service.EstablishementService {


	@Autowired
	private StorageServiceImpl storageService ;
	
	
	
    public CustomisedResponse getEstablishementInfoBySiret(String siret) throws ApiResponseException{
        RestTemplate restTemplate = new RestTemplate();
        ResponseDto response = new ResponseDto();
        try {
        response = restTemplate.getForObject("https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/" + siret, ResponseDto.class);
        }catch (HttpClientErrorException e) {
			e.printStackTrace();
			e.fillInStackTrace();
			e.getMessage();
			e.getStatusCode();
			throw new ApiResponseException("establishement not found");
		}
        return  mapper.responseCutomiser(response) ; 		
    }
    
    public List<CustomisedResponse> getAllEstablishementInfoBySiret(){
    	RequestDto siretDto = new RequestDto();
        List<CustomisedResponse> responseList = new ArrayList<>();
        for (String siret : siretDto.getSiretList()) {
        	responseList.add(this.getEstablishementInfoBySiret(siret));
		}
        try {
			storageService.saveResponseInCsvFile(responseList);
		} catch (IOException e) {
			e.printStackTrace();
		}        
        return responseList ; 
    }
}
