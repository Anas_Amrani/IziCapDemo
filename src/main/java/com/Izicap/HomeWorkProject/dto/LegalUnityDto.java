package com.Izicap.HomeWorkProject.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class LegalUnityDto {

    @JsonProperty("nom")
    private String name ;

    @JsonProperty("nom_usage")
    private String usageName ;
    
    @JsonProperty("denomination")
    private String denomination;
    
    @JsonProperty("numero_tva_intra")
    private String tvaNumber;
}
