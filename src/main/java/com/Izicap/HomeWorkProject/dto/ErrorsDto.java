package com.Izicap.HomeWorkProject.dto;

import java.time.ZonedDateTime;
import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ErrorsDto {

	public ErrorsDto(String message, HttpStatus code, ZonedDateTime zonedDateTime) {
		super();
		this.message = message;
		this.code = code;
		this.timestamp = zonedDateTime;
	}

	@JsonProperty("message")
	private String message;
	
	@JsonProperty("code")
	private final HttpStatus code;

	@JsonProperty("timestamp")
	private ZonedDateTime timestamp;
}
