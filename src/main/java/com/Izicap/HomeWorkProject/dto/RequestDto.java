package com.Izicap.HomeWorkProject.dto;

import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RequestDto {
	
	private final List<String> siretList = Arrays.asList("31302979500017",
			"41339442000033",
			"41339442000090",
			"41339442000116",
			"41776304200013",
			"43438147100011",
			"45251723800013",
			"52170053400014",
			"75254783600011",
			"97080195700014");

	public List<String> getSiretList() {
		return siretList;
	}


	@Override
	public String toString() {
		return "RequestDto [siretList=" + siretList + "]";
	}
}
