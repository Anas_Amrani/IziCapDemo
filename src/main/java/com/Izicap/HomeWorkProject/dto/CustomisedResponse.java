package com.Izicap.HomeWorkProject.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter
public class CustomisedResponse {

	@JsonProperty("id")
	private String id;

	@JsonProperty("nic")
	private String nic;
    
    @JsonProperty("Name")
    private String name;

    @JsonProperty("creation_Date")
    private String creationDate;

	@JsonProperty("full_Adress")
	private String fullAdress ;
	
    @JsonProperty("Tva_Number")
    private String tvaNumber;
}
