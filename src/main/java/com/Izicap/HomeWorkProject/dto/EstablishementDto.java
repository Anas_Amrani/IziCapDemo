package com.Izicap.HomeWorkProject.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class EstablishementDto {

	@JsonProperty("id")
	private String id;

	@JsonProperty("nic")
	private String nic;

	@JsonProperty("unite_legale")
	private LegalUnityDto legalUnity;

	@JsonProperty("date_creation")
	private Date creationDate;

	@JsonProperty("geo_adresse")
	private String geoAdress;

	@JsonProperty("longitude")
	private double longitude;

	@JsonProperty("latitude")
	private double latitude;

	@JsonProperty("complement_adresse")
	private String adressComplement;

}
