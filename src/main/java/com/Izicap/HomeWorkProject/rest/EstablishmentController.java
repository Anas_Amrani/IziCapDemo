package com.Izicap.HomeWorkProject.rest;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;
import com.Izicap.HomeWorkProject.dto.ErrorsDto;
import com.Izicap.HomeWorkProject.dto.ResponseDto;
import com.Izicap.HomeWorkProject.serviceImplementation.EstablishmentServiceImpl;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import java.util.List;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
public class EstablishmentController {

	@Autowired
	private EstablishmentServiceImpl establishmentService;

	//TODO ==> not forget to personalize dateCreation
	@Operation(summary = "Retreiving Company information by Siret Number", description = "Returns a Json Object with the company informations needed")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Success", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = CustomisedResponse.class)) }),
			@ApiResponse(responseCode = "404", description = "No Establishment Found"),
			@ApiResponse(responseCode = "500", description = "Service Not Available"),
			@ApiResponse(responseCode = "429", description = "MAximum Call Volum Exceeded")})
	@GetMapping("/infos/{siret}")
	public ResponseEntity<CustomisedResponse> getCompanyInfosBySiret(@PathVariable("siret") String siret) {
		return new ResponseEntity<CustomisedResponse>(establishmentService.getEstablishementInfoBySiret(siret),
				HttpStatus.ACCEPTED);
	}
	
	@Operation(summary = "Retreiving Companies informations by Siret Number List Given", description = "Returns a Json Object List with the companies informations needed and store it in local path C:\\\\iziCap_Output\\\\CompaniesInfos.csv")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Success", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = CustomisedResponse.class)) }),
			@ApiResponse(responseCode = "404", description = "No Establishment Found"),
			@ApiResponse(responseCode = "500", description = "Service Not Available"),
			@ApiResponse(responseCode = "429", description = "MAximum Call Volum Exceeded")})
	@GetMapping("/infos")
	public ResponseEntity<List<CustomisedResponse>> getAllCompanyInfosBySiret() {
		return new ResponseEntity<List<CustomisedResponse>>(establishmentService.getAllEstablishementInfoBySiret(),
				HttpStatus.ACCEPTED);
	}
}
