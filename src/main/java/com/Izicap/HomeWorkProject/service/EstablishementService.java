package com.Izicap.HomeWorkProject.service;

import java.util.List;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;
import com.Izicap.HomeWorkProject.exceptionHandler.ApiResponseException;

public interface EstablishementService {

	public static final com.Izicap.HomeWorkProject.mapper.ResponseMapper mapper = new com.Izicap.HomeWorkProject.mapper.ResponseMapper();
	CustomisedResponse getEstablishementInfoBySiret(String siret) throws ApiResponseException;
	List<CustomisedResponse> getAllEstablishementInfoBySiret()throws ApiResponseException;
}
