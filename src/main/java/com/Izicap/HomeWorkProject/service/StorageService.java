package com.Izicap.HomeWorkProject.service;

import java.io.IOException;
import java.util.List;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;

public interface StorageService {

	void saveResponseInCsvFile(List<CustomisedResponse> response) throws IOException;
}
