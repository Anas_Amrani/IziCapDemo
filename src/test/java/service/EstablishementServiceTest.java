package service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;
import com.Izicap.HomeWorkProject.dto.EstablishementDto;
import com.Izicap.HomeWorkProject.dto.LegalUnityDto;
import com.Izicap.HomeWorkProject.dto.ResponseDto;
import com.Izicap.HomeWorkProject.mapper.ResponseMapper;

public class EstablishementServiceTest {

	@Test
	public void getEstablishementInfoBySiret() throws JSONException, MalformedURLException, IOException, ParseException{

        ResponseMapper mapper = new ResponseMapper();

        String date_string = "1994-07-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        ;
        LegalUnityDto legalUnity = LegalUnityDto.builder().name("").usageName("").denomination("GROUPE ZANNIER PRESTATIONS")
                .tvaNumber("FR50380798959").build();

        EstablishementDto establishment = EstablishementDto.builder().legalUnity(legalUnity).id("450508987").nic("01122")
                .creationDate(formatter.parse(date_string)).geoAdress("33 Av des Martyrs de la Resistance 10000 Troyes")
                .longitude(2.349113).latitude(48.873447).adressComplement("").build();

        ResponseDto expected = ResponseDto.builder().establishement(establishment).build();

        RestTemplate restTemplate = new RestTemplate();
        ResponseDto response = new ResponseDto();
        response = restTemplate.getForObject(
                "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/38079895901122", ResponseDto.class);

        CustomisedResponse r1 = mapper.responseCutomiser(response);
        CustomisedResponse r2 = mapper.responseCutomiser(expected);

        assertEquals(r1.getId(), r2.getId());
        assertEquals(r1.getNic(), r2.getNic());
        assertEquals(r1.getName(), r2.getName());
        assertEquals(r1.getCreationDate(), r2.getCreationDate());
        assertEquals(r1.getFullAdress(), r2.getFullAdress());
        assertEquals(r1.getTvaNumber(), r2.getTvaNumber());
	}
}
