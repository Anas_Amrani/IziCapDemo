package mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.Test;

import com.Izicap.HomeWorkProject.dto.CustomisedResponse;
import com.Izicap.HomeWorkProject.dto.EstablishementDto;
import com.Izicap.HomeWorkProject.dto.LegalUnityDto;
import com.Izicap.HomeWorkProject.dto.ResponseDto;
import com.Izicap.HomeWorkProject.mapper.ResponseMapper;

public class ResponseMapperTest {

	
	ResponseMapper mapper = new ResponseMapper() ; 
	
	@Test
	public void responseMapperTest() {
		ResponseDto responseDto = new ResponseDto(new EstablishementDto("2", "1", new LegalUnityDto(), new Date(), "tst", 2.0, 2.0, "tst"));
		CustomisedResponse custom = mapper.responseCutomiser(responseDto);
		
		assertEquals(responseDto.getEstablishement().getId(), custom.getId());
		assertEquals(responseDto.getEstablishement().getNic(), custom.getNic());
		assertEquals(responseDto.getEstablishement().getLegalUnity().getTvaNumber(), custom.getTvaNumber());
		
	}
}
