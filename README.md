# Izicap Retreiving Establishment Infos

this is a java microService that querry an external api to get either an Estblishment infos by a given Siret Number Or a list of Estblishments given in the requestDto which will be stored in a local csvFile

# Prerequies to run the Application 
JDK : 17 
MVN : apache-maven-3.8.5

You can Run the application with one of the two following options :

1 => as the microservice is configured to be encapsulated in a docker container, you can run it by cloning the project change directory to the project folder on your local and run the commands below line by line 

docker build -t izicap.jar .  ( to create docker image)

docker run -p 8080:8080 izicap.jar ( to run the app ) 

PS : the port 8080 was configured in dockerFile you can change it to another if yours is occupied 

must have : Docker Desktop so you can visualize the created container and image 

2 => Clone the project on your local, open it in your favorite ide and run it as a spring boot Application

PS : if you ever face some difficulties with getters and setters, make sure that lombock is well configured and supported in your IDe 

You can also have a look at what Our controller looks like by accessing : http://localhost:8080/swagger-ui.html where you can also test our two getRequests

PS : to store the data locally, a csv file wille be generated in "src/main/resources/generated/companiesInfos.csv" 


# Decisions Highlight
As for my choices here are some of the features i used in my Application 

=> Spring Boot 2.7.0 

=> openCsv to help store data in a csvFile 

=> SpringDoc OpenApi {this was the best choice because of the incompatibility between springFox-swagger and spring boot 2.7.0 }

=> Lombok for Generating getter/setters Constructors ...

=> Junit jupiter For Unit Testing 

# Test Scenarios 

As mentioned below, to test the application you can access http://localhost:8080/swagger-ui.html while the app is running on the server 
and test the two methods we have : 

=> 	/company/infos/{siret}  which returns the informations of establishment with the given Siret Number 
You can try a valid siret number to see the service Response   EX: 41339442000033 
You can try an invalid siret Number to see the error message returned EX: 1235641
In case the siret number was not found an exception with the message "Establishement Not found will be raised" will be raised 

=> /company/infos/ which returns a list of establishements informations and saves it in a csv local file located in src/main/resources/generated
the list of siret Numbers was defined in the java Object RequestDto in the dto package 
as for the fir method if one of the siret Numbers given is not found an exception with a customised message will thrown 

Note : AjenkinsFile was provided to help build the application with jenkins.

