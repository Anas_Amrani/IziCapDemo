FROM openjdk:17
ADD target/izicap.jar izicap.jar
ENTRYPOINT [ "java","-jar","/izicap.jar" ]
EXPOSE 8080